package com.okeys.coderush;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {

    public static final String WAS_TILE_ADDED = "was_tile_added";
    private final SharedPreferences mPref;

    public Preferences(Context context) {
        mPref = PreferenceManager.getDefaultSharedPreferences(context);
    }

    boolean wasTileAdded(){
        return mPref.getBoolean(WAS_TILE_ADDED, false);
    }


    void setTileIsAdded(){
        mPref.edit().putBoolean(WAS_TILE_ADDED, true).apply();
    }
}
