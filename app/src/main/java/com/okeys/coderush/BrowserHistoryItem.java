package com.okeys.coderush;

/**
 * Created by rkokorev on 8/8/15.
 */
public class BrowserHistoryItem {

    private String mTitle;
    private String mUrl;

    public BrowserHistoryItem(String title, String url) {
        mTitle = title;
        mUrl =url;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }
}
