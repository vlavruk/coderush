package com.okeys.coderush;

import android.app.PendingIntent;
import android.app.WallpaperManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import cyanogenmod.app.CMStatusBarManager;
import cyanogenmod.app.CustomTile;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final List<String> APP_IGNORE_LIST = Arrays.asList("com.android.documentsui", "android",
                    "com.cyanogenmod.updater", "com.cyanogenmod.trebuchet", "com.android.systemui",
                    "com.okeys.coderush");

    private static final int CUSTOM_TILE_ID = 1;

    private static final int MODE_SET_UP = 1;
    private static final int MODE_RECENTS = 2;

    public static final String ACTION_OPEN_RECENTS = "com.okeys.coderush.ACTION_OPEN_RECENTS";
    private static final String TAG = MainActivity.class.getSimpleName();
    public static final int REMOTE_VIEW_TILES_COUNT = 9;

    private boolean mCallSettings = false;
    private StatsAdapter mStatsAdapter;
    private GridView mGridView;
    private View mRootView;
    private Preferences mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = new Preferences(this);

        if (!mPref.wasTileAdded()) {
            mPref.setTileIsAdded();
        }

        // adding anyway for demo
        // addTile();
        addRemoteViewTile();

        setContentView(R.layout.activity_main);

        mRootView = findViewById(R.id.root);

        mStatsAdapter = new StatsAdapter(this);
        mGridView = (GridView) findViewById(R.id.grid);
        mGridView.setAdapter(mStatsAdapter);

        mGridView.setOnItemClickListener(this);
        mGridView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mGridView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                setupGridView();
            }
        });

        mRootView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                final WallpaperManager wallpaperManager = WallpaperManager.getInstance(MainActivity.this);
                final Drawable wallpaperDrawable = wallpaperManager.getDrawable();

                final Bitmap bitmap;
                if (wallpaperDrawable.getIntrinsicWidth() <= 0 || wallpaperDrawable.getIntrinsicHeight() <= 0) {
                    bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created
                    // of 1x1
                    // pixel
                } else {
                    bitmap = Bitmap.createBitmap(wallpaperDrawable.getIntrinsicWidth(),
                                    wallpaperDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
                }

                Canvas canvas = new Canvas(bitmap);
                wallpaperDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                wallpaperDrawable.draw(canvas);

                blur(bitmap, mRootView);
                mRootView.getViewTreeObserver().removeOnPreDrawListener(this);
                return true;
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null && ACTION_OPEN_RECENTS.equals(intent.getAction())) {
            Toast.makeText(this, "Works", Toast.LENGTH_SHORT).show();
        }
    }

    private void addLaunchAppTile() {

        // Define an intent that has an action of toggling a state
        Intent intent = new Intent();
        intent.setAction(ACTION_OPEN_RECENTS);

        // Retrieve a pending intent from the system to be fired when the
        // clicks the custom tile
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Instantiate a builder object
        CustomTile customTile = new CustomTile.Builder(this).setOnClickIntent(pendingIntent)
        // set the pending intent
                        .setContentDescription("Show recents").setLabel("Recents")
                        // .shouldCollapsePanel(false)
                        .setIcon(R.drawable.ic_recents).build(); // build

        // Publish our tile to the status bar panel with CUSTOM_TILE_ID defined elsewhere
        CMStatusBarManager.getInstance(this).publishTile(CUSTOM_TILE_ID, customTile);

    }

    private void addRemoteViewTile() {
        // Create a remoteviews object
        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.remote_view);

        List<UsageStats> usageStatsForAYear = getUsageStatsForAYear();
        if (!usageStatsForAYear.isEmpty()) {
            for (int i = 0; i < REMOTE_VIEW_TILES_COUNT; i++) {
                int viewId = getResources().getIdentifier("item_" + i, "id", getPackageName());
                if (usageStatsForAYear.size() > i) {
                    UsageStats item = usageStatsForAYear.get(i);
                    contentView.setImageViewBitmap(viewId, getApplicationIconBitmap(item.getPackageName()));

                    Intent intentForPackage = getPackageManager().getLaunchIntentForPackage(item.getPackageName());
                    if (intentForPackage != null) {
                        contentView.setOnClickPendingIntent(viewId,
                                        PendingIntent.getActivity(this, 0, intentForPackage, 0));
                    }
                } else {
                    contentView.setViewVisibility(viewId, View.GONE);
                }
            }
        }

        // Create the new RemoteExpandedStyle
        CustomTile.RemoteExpandedStyle remoteExpandedStyle = new CustomTile.RemoteExpandedStyle();
        remoteExpandedStyle.setRemoteViews(contentView);

        // Build the custom tile
        CustomTile customTile = new CustomTile.Builder(this).setLabel("Recents").setIcon(R.drawable.ic_recents)
                        .setExpandedStyle(remoteExpandedStyle)
                        // .setOnSettingsClickIntent(
                        // new Intent(this, DummyClass.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
                        .setContentDescription("Show recents").build();

        // Publish the custom tile
        CMStatusBarManager.getInstance(this).publishTile(CUSTOM_TILE_ID, customTile);
    }

    private Bitmap getApplicationIconBitmap(String packageName) {
        Drawable icon = null;
        try {
            icon = getPackageManager().getApplicationIcon(packageName);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, e.getMessage(), e);
        }

        if (icon != null) {
            return drawableToBitmap(icon);
        }
        return null;
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        int width = drawable.getIntrinsicWidth();
        width = width > 0 ? width : 1;
        int height = drawable.getIntrinsicHeight();
        height = height > 0 ? height : 1;

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private void setupGridView() {
        mGridView.getLayoutParams().height = (int) (mRootView.getHeight() * 0.8f);
        // int itemWidth = mGridView.getWidth() / mGridView.getNumColumns();
        mStatsAdapter.updateItemSize(mGridView.getColumnWidth());
    }

    @Override
    protected void onResume() {
        super.onResume();
        final List<UsageStats> queryUsageStats = getUsageStatsForAYear();
        mStatsAdapter.setItems(queryUsageStats);

    }

    private List<UsageStats> getUsageStatsForAYear() {
        final UsageStatsManager usageStatsManager = getUsageStatsManager();
        Calendar cal = Calendar.getInstance();
        long current = cal.getTimeInMillis();
        cal.add(Calendar.YEAR, -1);
        long yearBefore = cal.getTimeInMillis();

        List<UsageStats> queryUsageStats = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_BEST,
                        yearBefore, current);

        final List<UsageStats> filteredUsageStats = new ArrayList<>();
        if (queryUsageStats.isEmpty()) {
            // probably don't have an access
            callSettings();
        } else {
            for (UsageStats usageStats : queryUsageStats) {
                if (!APP_IGNORE_LIST.contains(usageStats.getPackageName()) && haveIntent(usageStats.getPackageName())) {
                    filteredUsageStats.add(usageStats);
                }
            }
        }

        Collections.sort(filteredUsageStats, new Comparator<UsageStats>() {
            @Override
            public int compare(UsageStats lhs, UsageStats rhs) {
                if (lhs.getLastTimeStamp() > rhs.getLastTimeStamp()) {
                    return -1;
                } else if (lhs.getLastTimeStamp() < rhs.getLastTimeStamp()) {
                    return 1;
                }
                return 0;
            }
        });

        return filteredUsageStats;
    }

    private boolean haveIntent(String packageName) {
        return getPackageManager().getLaunchIntentForPackage(packageName) != null;
    }

    private UsageStatsManager getUsageStatsManager() {
        return (UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE);
    }

    private String logUsageStat(UsageStats queryUsageStat) {
        String firstTime = DateFormat.getDateTimeInstance().format(new Date(queryUsageStat.getFirstTimeStamp()));
        String lastTime = DateFormat.getDateTimeInstance().format(new Date(queryUsageStat.getLastTimeStamp()));
        return queryUsageStat.getPackageName() + "-> first: " + firstTime + ", last: " + lastTime;
    }

    void callSettings() {
        mCallSettings = true;
        Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
        startActivity(intent);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        UsageStats item = mStatsAdapter.getItem(position);

        Intent launchIntentForPackage = getPackageManager().getLaunchIntentForPackage(item.getPackageName());

        if (launchIntentForPackage != null) {
            startActivity(launchIntentForPackage);
        } else {
            Toast.makeText(this, "Can't find an activity to start!", Toast.LENGTH_LONG).show();
        }
    }

    static class AppViewHolder {

        private static final String TAG = AppViewHolder.class.getSimpleName();

        private final ImageView mView;
        private final Context mContext;

        public AppViewHolder(Context context) {
            mContext = context;
            mView = new ImageView(context);
            mView.setScaleType(ImageView.ScaleType.CENTER);
            mView.setTag(this);
        }

        View getView() {
            return mView;
        }

        public void bindView(UsageStats item) {
            Drawable icon = null;
            try {
                icon = mContext.getPackageManager().getApplicationIcon(item.getPackageName());
            } catch (PackageManager.NameNotFoundException e) {
                Log.e(TAG, e.getMessage(), e);
            }
            mView.setImageDrawable(icon);
        }
    }

    private void blur(Bitmap bkg, View view) {
        float scaleFactor = 8;
        float radius = 4;

        Bitmap overlay = Bitmap.createBitmap((int) (view.getWidth() / scaleFactor),
                        (int) (view.getHeight() / scaleFactor), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(overlay);
        canvas.translate(-view.getLeft() / scaleFactor, -view.getTop() / scaleFactor);
        canvas.scale(1 / scaleFactor, 1 / scaleFactor);
        Paint paint = new Paint();
        paint.setFlags(Paint.FILTER_BITMAP_FLAG);
        canvas.drawBitmap(bkg, 0, 0, paint);

        overlay = FastBlur.fastblur(overlay, (int) radius);

        view.setBackground(new BitmapDrawable(getResources(), overlay));
    }
}
