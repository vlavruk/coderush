package com.okeys.coderush;

import android.app.usage.UsageStats;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.okeys.coderush.utils.BindableAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vlavruk on 8/8/15.
 */
class StatsAdapter extends BindableAdapter<UsageStats> {

    List<UsageStats> mItems = new ArrayList<>();
    private int mItemWidth;

    public StatsAdapter(Context context) {
        super(context);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public UsageStats getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View newView(LayoutInflater inflater, int position, ViewGroup container) {
        return new MainActivity.AppViewHolder(getContext()).getView();
    }

    @Override
    public void bindView(UsageStats item, int position, View view) {
        ((MainActivity.AppViewHolder) view.getTag()).bindView(item);
    }

    public void setItems(List<UsageStats> queryUsageStats) {
        mItems.clear();
        mItems.addAll(queryUsageStats);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View view, ViewGroup container) {
        View getView = super.getView(position, view, container);
        ViewGroup.LayoutParams layoutParams = getView.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new GridView.LayoutParams(mItemWidth, mItemWidth);
            getView.setLayoutParams(layoutParams);
        } else {
            layoutParams.height = mItemWidth;
        }
        return getView;
    }

    public void updateItemSize(int itemWidth) {
        mItemWidth = itemWidth;
    }
}
