package com.okeys.coderush;

public class CallLogItem {

    private String mName;
    private String mPhone;
    /**
     * {@link android.provider.CallLog.Calls}
     */
    private int mType;

    public CallLogItem(String name, String phone, int type) {
        mName = name;
        mPhone = phone;
        mType = type;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        mType = type;
    }
}
