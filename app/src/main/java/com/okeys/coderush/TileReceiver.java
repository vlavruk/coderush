package com.okeys.coderush;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class TileReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (MainActivity.ACTION_OPEN_RECENTS.equals(intent.getAction())) {
            Intent activityIntent = new Intent(context, MainActivity.class);
            activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(activityIntent);
        }
    }
}
