package com.okeys.coderush;

import android.content.Context;
import android.database.Cursor;
import android.provider.Browser;

import java.util.ArrayList;
import java.util.List;

public class BrowserHistoryProvider {

    public static List<BrowserHistoryItem> getBrowserHistory(Context context) {
        List<BrowserHistoryItem> items = new ArrayList<>();
        Cursor mCur = context.getContentResolver().query(Browser.BOOKMARKS_URI, Browser.HISTORY_PROJECTION, null, null,
                        null);
        mCur.moveToFirst();
        if (mCur.moveToFirst() && mCur.getCount() > 0) {
            while (!mCur.isAfterLast()) {
                String title = mCur.getString(Browser.HISTORY_PROJECTION_TITLE_INDEX);
                String url = mCur.getString(Browser.HISTORY_PROJECTION_URL_INDEX);
                items.add(new BrowserHistoryItem(title, url));
                mCur.moveToNext();
            }
        }
        return items;
    }
}
